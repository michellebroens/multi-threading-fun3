public class OnOffBit {
    public static void main(String args[]) throws Exception {
        byte bit = 0;
        for (int i = 0; i < 20; i += 2) {
            System.out.println("Value of i: " + i + ", value of bit: " + bit);
            switch (bit) {
                case 0:
                    bit = 1;
                    break;
                case 1:
                    bit = 0;
                    break;
            }
        }

        System.out.println("");

        for (int i = 0; i < 20; i += 2) {
            System.out.println("Value of i: " + i + ", value of bit: " + bit);

            bit = (byte) (bit == 0 ? 1 : 0);
        }



    }
}
