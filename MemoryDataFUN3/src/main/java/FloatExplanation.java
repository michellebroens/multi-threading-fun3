public class FloatExplanation {

    public static void main(String args[]) throws Exception {
        Example1();
        Example2();
    }

    public static void Example1() {
        int i = 42;
        float f = 1.0f/i;
        double d = 1.0/i;

        // The float only holds 7 digits, which makes the calculations different
        System.out.println("Float: " + f + ": " + i*f + " - Double: " + d + ":  "+  i*d);

        System.out.println("Result with 42 float: " + (i*f==1.0f));
        System.out.println("Result with 42 double: " + (i*d==1.0d));
    }

    public static void Example2() {
        int i = 41;
        float f = 1.0f/i;
        double d = 1.0/i;

        System.out.println("Float: " + f + ": " + i*f + " - Double: " + d + ":  "+  i*d);

        System.out.println("Result with 41 float: " + (i*f==1.0f));
        System.out.println("Result with 41 double: " + (i*d==1.0d));
    }
}
