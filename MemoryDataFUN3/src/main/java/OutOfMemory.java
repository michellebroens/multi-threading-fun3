import java.util.*;

public class OutOfMemory {

    public static void main(String args[]) throws Exception
    {
        int dummyValue = 15;
        long[] array = null;

        while (true) {
            array = new long[dummyValue];

            dummyValue *= dummyValue * 2;
            Thread.sleep(500);
            System.out.println("Current value: " + dummyValue);
        }
    }
}
